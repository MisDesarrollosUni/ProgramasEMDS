package Examen;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class TestPruebaExamen {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        System.setProperty("webdriver.chrome.driver","C:\\Users\\hecto\\Escritorio\\Universidad\\ProgramasEMDS\\PruebasAutomatizadas\\utlidades\\chromedriver.exe");
        driver = new ChromeDriver();
        baseUrl = "https://www.google.com/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void testPruebaExamen() throws Exception {
        driver.get("http://desarrollosenweb.com/web5/index.php");
        driver.findElement(By.id("usuario")).click();
        driver.findElement(By.id("usuario")).clear();
        driver.findElement(By.id("usuario")).sendKeys("admin");
        driver.findElement(By.id("password")).clear();
        driver.findElement(By.id("password")).sendKeys("grupoC");
        driver.findElement(By.id("entrarSistema")).click();
        driver.findElement(By.linkText("Administrar usuarios")).click();
        driver.findElement(By.id("nombre")).click();
        driver.findElement(By.id("nombre")).clear();
        driver.findElement(By.id("nombre")).sendKeys("Hector");
        driver.findElement(By.id("apellido")).click();
        driver.findElement(By.id("apellido")).clear();
        driver.findElement(By.id("apellido")).sendKeys("Saldaña");
        driver.findElement(By.id("frmRegistro")).click();
        driver.findElement(By.id("usuario")).click();
        driver.findElement(By.id("usuario")).clear();
        driver.findElement(By.id("usuario")).sendKeys("HectorSaldes");
        driver.findElement(By.id("password")).click();
        driver.findElement(By.id("password")).clear();
        driver.findElement(By.id("password")).sendKeys("123");
        driver.findElement(By.id("registro")).click();
        driver.findElement(By.linkText("Usuario: admin")).click();
        driver.findElement(By.linkText("Salir")).click();
        driver.findElement(By.id("usuario")).click();
        driver.findElement(By.id("usuario")).clear();
        driver.findElement(By.id("usuario")).sendKeys("HectorSaldes");
        driver.findElement(By.id("password")).click();
        driver.findElement(By.id("password")).clear();
        driver.findElement(By.id("password")).sendKeys("123");
        driver.findElement(By.id("entrarSistema")).click();
        driver.findElement(By.linkText("Administrar Articulos")).click();
        driver.findElement(By.linkText("Categorias")).click();
        driver.findElement(By.id("categoria")).click();
        driver.findElement(By.id("categoria")).clear();
        driver.findElement(By.id("categoria")).sendKeys("Hector_Necaxa2");
        driver.findElement(By.id("btnAgregaCategoria")).click();
        driver.findElement(By.linkText("Clientes")).click();
        driver.findElement(By.id("nombre")).click();
        driver.findElement(By.id("nombre")).clear();
        driver.findElement(By.id("nombre")).sendKeys("nombre_hectorss");
        driver.findElement(By.id("apellidos")).click();
        driver.findElement(By.id("apellidos")).clear();
        driver.findElement(By.id("apellidos")).sendKeys("apellido_hectorss");
        driver.findElement(By.id("direccion")).click();
        driver.findElement(By.id("direccion")).clear();
        driver.findElement(By.id("direccion")).sendKeys("direccion_hectorss");
        driver.findElement(By.id("email")).click();
        driver.findElement(By.id("email")).clear();
        driver.findElement(By.id("email")).sendKeys("email_hectorss");
        driver.findElement(By.id("telefono")).click();
        driver.findElement(By.id("telefono")).clear();
        driver.findElement(By.id("telefono")).sendKeys("telefono_hectorss");
        driver.findElement(By.id("rfc")).click();
        driver.findElement(By.id("rfc")).clear();
        driver.findElement(By.id("rfc")).sendKeys("rfc_hectorss");
        driver.findElement(By.id("btnAgregarCliente")).click();
        driver.findElement(By.linkText("Administrar Articulos")).click();
        driver.findElement(By.linkText("Articulos")).click();
        driver.findElement(By.id("categoriaSelect")).click();
        new Select(driver.findElement(By.id("categoriaSelect"))).selectByVisibleText("Hector_Necaxa2");
        driver.findElement(By.id("categoriaSelect")).click();
        driver.findElement(By.id("nombre")).click();
        driver.findElement(By.id("nombre")).clear();
        driver.findElement(By.id("nombre")).sendKeys("hectorsaldana");
        driver.findElement(By.id("descripcion")).click();
        driver.findElement(By.id("descripcion")).clear();
        driver.findElement(By.id("descripcion")).sendKeys("muy lejos");
        driver.findElement(By.id("cantidad")).click();
        driver.findElement(By.id("cantidad")).clear();
        driver.findElement(By.id("cantidad")).sendKeys("0");
        driver.findElement(By.id("precio")).click();
        driver.findElement(By.id("precio")).clear();
        driver.findElement(By.id("precio")).sendKeys("0");
        driver.findElement(By.id("btnAgregaArticulo")).click();
        driver.findElement(By.id("imagen")).click();
        driver.findElement(By.id("imagen")).clear();
        driver.findElement(By.id("imagen")).sendKeys("C:\\fakepath\\vlad-zaytsev-Mqn0DCfzBjM-unsplash.jpg");
        driver.findElement(By.id("btnAgregaArticulo")).click();
        driver.findElement(By.linkText("Usuario: HectorSaldes")).click();
        driver.findElement(By.linkText("Salir")).click();
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
