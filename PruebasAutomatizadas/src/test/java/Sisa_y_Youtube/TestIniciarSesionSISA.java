package Sisa_y_Youtube;


import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestIniciarSesionSISA {
	private WebDriver driver;
	private String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\hecto\\Escritorio\\Universidad\\ProgramasEMDS\\PruebasAutomatizadas\\utlidades\\chromedriver.exe");
		driver = new ChromeDriver();
		baseUrl = "https://www.google.com/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void testIniciarSesionSISA() throws Exception {
		//INICIA EN SISA
		driver.get("https://srvcldutez.utez.edu.mx:8443/SISAVA/");
		driver.findElement(By.name("usuario.nick")).click();
		driver.findElement(By.name("usuario.nick")).clear();
		// INGRESA  LA MATRICULA
		driver.findElement(By.name("usuario.nick")).sendKeys("20193tn070");
		driver.findElement(By.name("usuario.contrasexa")).clear();
		// INGRESA LA CONTRASEÑA
		driver.findElement(By.name("usuario.contrasexa")).sendKeys("CA8");
		// INICIA SESION
		driver.findElement(By.xpath("//html[@id='ng-app']/body/div[2]/div/div/div[2]/div[2]/form/fieldset/button")).click();

		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);


		// VOY AL HISTORIAL ACADEMICO
		driver.findElement(By.linkText("Historial académico")).click();

		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

	private boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	private boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	private String closeAlertAndGetItsText() {
		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}
	}
}
