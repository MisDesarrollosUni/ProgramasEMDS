package Sisa_y_Youtube;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class TestVideoYoutube {
	private WebDriver driver;
	private String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\hecto\\Escritorio\\Universidad\\ProgramasEMDS\\PruebasAutomatizadas\\utlidades\\chromedriver.exe");
		driver = new ChromeDriver();
		baseUrl = "https://www.google.com/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void testVideoYoutube() throws Exception {
		// ABRE YOUTUBE
		driver.get("https://www.youtube.com/");
		//INICIA SESIÓN
		driver.findElement(By.xpath("(//yt-formatted-string[@id='text'])[2]")).click();
		// INGRESA CORREO
		driver.findElement(By.id("identifierId")).sendKeys("20193tn070@utez.edu.mx");
		driver.findElement(By.id("identifierId")).sendKeys(Keys.ENTER); //ENTER
		driver.findElement(By.name("password")).click();
		driver.findElement(By.name("password")).clear();
		// INGRESA CONTRASEÑA
		driver.findElement(By.name("password")).sendKeys("20193tn070");
		//ENTER
		driver.findElement(By.xpath("//div[@id='passwordNext']/div/button/div[2]")).click();
		driver.findElement(By.name("search_query")).click();
		driver.findElement(By.name("search_query")).click();
		driver.findElement(By.name("search_query")).clear();
		//BUSCA EN YOUTUBE
		driver.findElement(By.name("search_query")).sendKeys("CDS-UTEZ-TD-4TIC2018");
		driver.findElement(By.id("search-form")).submit();
		// SELECCIONA EL VIDEO
		driver.findElement(By.xpath("//yt-formatted-string[contains(text(),'CDS-UTEZ-TD-4TIC2018')]")).click();
		// PAUSA EL VIDEO
		driver.findElement(By.xpath("//div[@id='movie_player']/div[24]/div[2]/div/button")).click();
		// MAXIMIZA EL VIDEO
		driver.findElement(By.xpath("//div[@id='movie_player']/div[24]/div[2]/div[2]/button[9]")).click();
		//MINIMIZA EL VIDEO
		driver.findElement(By.xpath("//div[@id='movie_player']/div[24]/div[2]/div[2]/button[9]")).click();
		// DA LIKE AL VIDEO
		driver.findElement(By.xpath("//body/ytd-app[1]/div[1]/ytd-page-manager[1]/ytd-watch-flexy[1]/div[4]/div[1]/div[1]/div[5]/div[2]/ytd-video-primary-info-renderer[1]/div[1]/div[1]/div[3]/div[1]/ytd-menu-renderer[1]/div[1]/ytd-toggle-button-renderer[1]/a[1]/yt-icon-button[1]/button[1]/yt-icon[1]")).click();
		// CIERRA LA SESION
		driver.findElement(By.id("img")).click();
		driver.findElement(By.xpath("(//yt-formatted-string[@id='label'])[5]")).click();
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

	private boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	private boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	private String closeAlertAndGetItsText() {
		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}
	}
}
