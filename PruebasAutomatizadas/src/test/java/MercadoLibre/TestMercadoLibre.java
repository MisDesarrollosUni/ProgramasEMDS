package MercadoLibre;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class TestMercadoLibre {
	private WebDriver driver;
	private String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\hecto\\Escritorio\\Universidad\\ProgramasEMDS\\PruebasAutomatizadas\\utlidades\\chromedriver.exe");
		driver = new ChromeDriver();
		baseUrl = "https://www.google.com/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void testMercadoLibre() throws Exception {
		driver.get("https://www.google.com/");
		driver.findElement(By.name("q")).clear();
		driver.findElement(By.name("q")).sendKeys("mercado libre");
		driver.findElement(By.id("tsf")).submit();
		driver.findElement(By.xpath("/html[1]/body[1]/div[7]/div[2]/div[10]/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/a[1]/h3[1]/span[1]")).click();
		driver.findElement(By.name("as_word")).click();
		driver.findElement(By.name("as_word")).clear();
		driver.findElement(By.name("as_word")).sendKeys("pantalla sony 55 pulgadas");
		driver.findElement(By.name("as_word")).sendKeys(Keys.ENTER);

		driver.findElement(By.xpath("//body/main[@id='root-app']/div[1]/div[1]/section[1]/ol[1]/li[15]/div[1]/div[1]/div[1]/a[1]/div[1]/div[1]/div[1]/div[1]/div[1]/img[1]")).click();

		driver.findElement(By.xpath("//body/main[@id='root-app']/div[1]/div[3]/div[1]/div[2]/div[1]/div[1]/div[8]/div[1]/div[1]/div[1]/button[1]/span[1]/span[3]/*[1]")).click();

		//DE AQUÍ HACIA ABAJO NO ENCUENTRA LAS RUTAS DEL XPATH CON LA EXTENSIÓN CROPATH NI CON LAS DE SELENIUM O KATALON :C

		driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[3]/div[1]/ul[1]/li[5]/button[1]")).click();

		driver.findElement(By.xpath("/html[1]/body[1]/main[1]/div[1]/div[3]/div[1]/div[2]/div[1]/div[1]/div[8]/form[1]/div[1]/button[2]/span[1]")).click();

		driver.findElement(By.xpath("//body/main[@id='root-app']/div[1]/div[1]/div[1]/div[2]/div[2]/a[1]")).click();

		driver.findElement(By.xpath("//body[1]/main[1]/div[1]/div[1]/div[2]/div[4]/div[1]/div[1]/div[1]/article[1]/div[3]/ul[1]/li[1]/form[1]/input[2]")).click();
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

	private boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	private boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	private String closeAlertAndGetItsText() {
		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}
	}
}
