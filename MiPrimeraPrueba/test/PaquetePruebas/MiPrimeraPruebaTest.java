/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PaquetePruebas;

import miprimeraprueba.MiPrimeraPrueba;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hector Saldaña
 */
public class MiPrimeraPruebaTest {

    public MiPrimeraPruebaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
// ESTA COMENATADO EL METODO IMPORTANTE
    /*@Test
     public void hello() {
        System.out.println("Hola Test!");
    }
     */
    MiPrimeraPrueba suma = new MiPrimeraPrueba();
    MiPrimeraPrueba multi = new MiPrimeraPrueba();
    MiPrimeraPrueba resta = new MiPrimeraPrueba();
    MiPrimeraPrueba div = new MiPrimeraPrueba();
    @Test
    public void testSumaNumeros1() {
        System.out.println("Test 1 para método suma números EXITO");
        int n1 = 8, n2 =2;      
        int resultadoE = 10;
        int resultadoO = suma.sumaNumeros(n1, n2);
        //assertEquals válida si es igual para la prueba
        assertEquals(resultadoE, resultadoO);
    }
    @Test
    public void testSumaNumeros2() {
        System.out.println("Test 2 para método suma números FALLO");
        int n1 = 5, n2 = 5;      
        int resultadoE = 20;
        int resultadoO = suma.sumaNumeros(n1, n2);
        //assertEquals válida si es igual para la prueba
        assertEquals(resultadoE, resultadoO);
    }
    @Test
    public void testSumaNumeros3() {
        System.out.println("Test 3 para método suma números FALLO");
        int n1 = 26, n2 = 10;      
        int resultadoE = 35;
        int resultadoO = suma.sumaNumeros(n1, n2);
        //assertEquals válida si es igual para la prueba
        assertEquals(resultadoE, resultadoO);
    }
    

    @Test
    public void testMultiplicacionNumeros1() {
        int n1, n2, resulEsperado, resulObtenido;
        System.out.println("Test 1 para método multiplicación números EXITO");
        n1 = 12;
        n2 = 2;
        resulEsperado = 24;
        resulObtenido = multi.multiplicacionNumeros(n1, n2);
        assertEquals(resulEsperado, resulObtenido);
    }

    @Test
    public void testMultiplicacionNumeros2() {
        int n1, n2, resulEsperado, resulObtenido;
        System.out.println("Test 2 para método multiplicación números FALLO");
        n1 = 12;
        n2 = 3;
        resulEsperado = 24;
        resulObtenido = multi.multiplicacionNumeros(n1, n2);
        assertEquals(resulEsperado, resulObtenido);
    }

    @Test
    public void testMultiplicacionNumeros3() {
        int n1, n2, resulEsperado, resulObtenido;
        System.out.println("Test 3 para método multiplicación números FALLO");
        n1 = 6;
        n2 = 9;
        resulEsperado = 18;
        resulObtenido = multi.multiplicacionNumeros(n1, n2);
        assertEquals(resulEsperado, resulObtenido);
    }

    /////////////////////////////////////////////////////////////
    @Test
    public void testRestaNumeros1() {
        int n1, n2, resulEsperado, resulObtenido;
        System.out.println("Test 1 para método resta números EXITO");
        n1 = 10;
        n2 = 5;
        resulEsperado = 5;
        resulObtenido = resta.restaNumeros(n1, n2);
        assertEquals(resulEsperado, resulObtenido);
    }

    @Test
    public void testRestaNumeros2() {
        int n1, n2, resulEsperado, resulObtenido;
        System.out.println("Test 2 para método resta números FALLO");
        n1 = 6;
        n2 = 3;
        resulEsperado = 4;
        resulObtenido = resta.restaNumeros(n1, n2);
        assertEquals(resulEsperado, resulObtenido);
    }

    @Test
    public void testRestaNumeros3() {
        int n1, n2, resulEsperado, resulObtenido;
        System.out.println("Test 3 para método resta números FALLO");
        n1 = 25;
        n2 = 24;
        resulEsperado = 5;
        resulObtenido = resta.restaNumeros(n1, n2);
        assertEquals(resulEsperado, resulObtenido);
    }

    /////////////////////////////////////////////////////////////
    @Test
    public void testDivisionNumeros1() {
        int n1, n2, resulEsperado, resulObtenido;
        System.out.println("Test 1 para método división números EXITO");
        n1 = 10;
        n2 = 5;
        resulEsperado = 2;
        resulObtenido = div.divisionNumeros(n1, n2);
        assertEquals(resulEsperado, resulObtenido);

    }

    @Test
    public void testDivisionNumeros2() {
        int n1, n2, resulEsperado, resulObtenido;
        System.out.println("Test 2 para método división números FALLO");
        n1 = 20;
        n2 = 20;
        resulEsperado = 3;
        resulObtenido = div.divisionNumeros(n1, n2);
        assertEquals(resulEsperado, resulObtenido);
    }

    @Test
    public void testDivisionNumeros3() {
        int n1, n2, resulEsperado, resulObtenido;
        System.out.println("Test 3 para método división números FALLO");
        n1 = 202;
        n2 = 2;
        resulEsperado = 100;
        resulObtenido = div.divisionNumeros(n1, n2);
        assertEquals(resulEsperado, resulObtenido);
    }
}
