package miprimeraprueba;

public class MiPrimeraPrueba {
    //Prueba para método de suma de dos números

    public int sumaNumeros(int num1, int num2) {
        int a = num1, b = num2;
        return a+b;
    }
    
    public int multiplicacionNumeros(int num1, int num2){
        return num1*num2;
    }
    
    public int restaNumeros(int num1, int num2){
        return num1 - num2;
    }
    
    public int divisionNumeros(int num1, int num2){
        return num1/num2;
    }

    public static void main(String[] args) {

    }

}
