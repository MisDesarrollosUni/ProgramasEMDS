/*
 * Saldaña Espinoza Hector 
 * 4C - DSM - UTEZ
 * Evaluación y mejora de Software
 */
package examenunidad2;

import java.util.Scanner;

/**
 *
 * @author Hector Saldaña
 */
public class IniciodeSesion {
     public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        String user, pass;
        System.out.print("Usuario: ");
        user = leer.next();
        System.out.print("Contraseña: ");
        pass = leer.next();
        if(validarInicio(user,pass)) System.out.println("\tBIENVENIDO"); else System.out.println("USUARIO Y CONTRASEÑA NO VÁLIDO");
                
    }
    
    public static boolean validarInicio(String user, String pass){
        if(user.equals("admin") && pass.equals("Adm1Ni23#")) return true; else return false;
    }
    
}
