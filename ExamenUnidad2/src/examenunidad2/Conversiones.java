/*
 * Saldaña Espinoza Hector 
 * 4C - DSM - UTEZ
 * Evaluación y mejora de Software
 */
package examenunidad2;

import java.util.Scanner;

/**
 *
 * @author Hector Saldaña
 */
public class Conversiones {
     public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        int opc = 0;
        double dato = 0, resultado = 0;
        do {
            System.out.println("-------------------------------------");
            System.out.println("MENÚ CONVERSIONES TEMPERATURA\n"
                    + "\t 1.- °F -> °C\n"
                    + "\t 2.- °K -> °C\n"
                    + "\t 3.- °K -> °F\n"
                    + "\t 4.- °C -> °F\n"
                    + "\t 5.- SALIR");
            System.out.print("Elige: ");
            opc = leer.nextInt();
            if (opc >= 1 && opc <= 4) {
                System.out.print("Ingrese los grados°: ");
                dato = leer.nextDouble();
            }
            switch (opc) {
                case 1:
                    resultado = deFahrenheitCelsius(dato);
                    break;
                case 2:
                    resultado = deKelvinCelsius(dato);
                    break;
                case 3:
                    resultado = deKelvinFahrenheit(dato);
                    break;
                case 4:
                    resultado = deCelsiusFahrenheit(dato);
                    break;
                case 5:
                    opc = 5;
                    System.out.println("\tHASTA LUEGO!");
                    break;
                default:
                    System.out.println("\tESA OPCIÓN NO EXISTE");
            }
            if (opc >= 1 && opc <= 4) {
                System.out.println("EL resutado es: " + resultado + ((opc == 1 || opc == 2) ? " °C" : " °F"));
            }
        } while (opc != 5);
    }

    public static double deFahrenheitCelsius(double dato) {
        return (dato - 32) * 5 / 9;
    }

    public static double deKelvinCelsius(double dato) {
        return dato - 273.15;
    }

    public static double deKelvinFahrenheit(double dato) {
        return (dato - 273.15) * 9 / 5 + 32;
    }

    public static double deCelsiusFahrenheit(double dato) {
        return (dato * 9 / 5) + 32;
    }
}
