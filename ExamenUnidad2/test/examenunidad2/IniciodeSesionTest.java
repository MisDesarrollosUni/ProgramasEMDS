/*
 * Saldaña Espinoza Hector 
 * 4C - DSM - UTEZ
 * Evaluación y mejora de Software
 */
package examenunidad2;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hector Saldaña
 */
public class IniciodeSesionTest {
    
    public IniciodeSesionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of main method, of class IniciodeSesion.
    
    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        IniciodeSesion.main(args);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

   
     * Test of validarInicio method, of class IniciodeSesion.
     */
    @Test
    public void testValidarInicio1() {
        System.out.println("validarInicio PRUEBA EXITOSA");
        String user = "admin";
        String pass = "Adm1Ni23#";
        boolean expResult = true;
        boolean result = IniciodeSesion.validarInicio(user, pass);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
     
    @Test
    public void testValidarInicio2() {
        System.out.println("validarInicio PRUEBA FALLIDA");
        String user = "Admin";
        String pass = "Adm1Ni23";
        boolean expResult = true;
        boolean result = IniciodeSesion.validarInicio(user, pass);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testValidarInicio3() {
        System.out.println("validarInicio PRUEBA FALLIDA");
        String user = "ADMIN";
        String pass = "123";
        boolean expResult = true;
        boolean result = IniciodeSesion.validarInicio(user, pass);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
     @Test
    public void testValidarInicio4() {
        System.out.println("validarInicio PRUEBA FALLIDA");
        String user = "admin";
        String pass = "admin";
        boolean expResult = true;
        boolean result = IniciodeSesion.validarInicio(user, pass);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
}
