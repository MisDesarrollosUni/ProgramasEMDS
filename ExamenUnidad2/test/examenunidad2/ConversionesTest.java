/*
 * Saldaña Espinoza Hector 
 * 4C - DSM - UTEZ
 * Evaluación y mejora de Software
 */
package examenunidad2;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hector Saldaña
 */
public class ConversionesTest {
    
    public ConversionesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of main method, of class Conversiones.
     
    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        Conversiones.main(args);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of deKelvinCelsius method, of class Conversiones.
     */
    @Test
    public void testDeFahrenheitCelsius1() {
        System.out.println("deFahrenheitCelsius PRUEBA EXITOSA");
        double dato = 188888;
        double expResult = 104920;
        double result = Conversiones.deFahrenheitCelsius(dato);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    @Test
    public void testDeFahrenheitCelsius2() {
        System.out.println("deFahrenheitCelsius PRUEBA FALLIDA");
        double dato = 1587;
        double expResult = 104920;
        double result = Conversiones.deFahrenheitCelsius(dato);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    

    /**
     * Test of deKelvinCelsius method, of class Conversiones.
     */
    @Test
    public void testDeKelvinCelsius1() {
        System.out.println("deKelvinCelsius PRUEBA EXITOSA");
        double dato = 0;
        double expResult = -273.15;
        double result = Conversiones.deKelvinCelsius(dato);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }
    
    @Test
    public void testDeKelvinCelsius2() {
        System.out.println("deKelvinCelsius PRUEBA FALLIDA");
        double dato = 785;
        double expResult = -4587;
        double result = Conversiones.deKelvinCelsius(dato);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    /**
     * Test of deKelvinFahrenheit method, of class Conversiones.
     */
    
     @Test
    public void testDeKelvinFahrenheit1() {
        System.out.println("deKelvinFahrenheit PRUEBA EXITOSA");
        double dato = 487;
        double expResult = 416.93;
        double result = Conversiones.deKelvinFahrenheit(dato);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }
    
    @Test
    public void testDeKelvinFahrenheit2() {
        System.out.println("deKelvinFahrenheit PRUEBA FALLIDA");
        double dato = 782;
        double expResult = 45.5;
        double result = Conversiones.deKelvinFahrenheit(dato);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    /**
     * Test of deCelsiusFahrenheit method, of class Conversiones.
     */
    
    @Test
    public void testDeCelsiusFahrenheit1() {
        System.out.println("deCelsiusFahrenheit PRUEBA EXITOSA");
        double dato = 78;
        double expResult = 172.4;
        double result = Conversiones.deCelsiusFahrenheit(dato);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testDeCelsiusFahrenheit2() {
        System.out.println("deCelsiusFahrenheit PRUEBA FALLIDA");
        double dato = 45;
        double expResult = -44;
        double result = Conversiones.deCelsiusFahrenheit(dato);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
