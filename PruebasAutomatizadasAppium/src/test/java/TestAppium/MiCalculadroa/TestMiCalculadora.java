package TestAppium.MiCalculadroa;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class TestMiCalculadora {
    static AndroidDriver<MobileElement> driver;

    /*
     * PRUEBAS A MI CALCULADROA
     */

    @Test
    public void abrirMiCalculadora() {
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability("deviceName", "ZY322NQQ55");
        cap.setCapability("platformName", "Android");
        cap.setCapability("platformVersion", "9");
        //cmp=mx.edu.utez.micalculadora/.MainActivity
        cap.setCapability("appPackage", "mx.edu.utez.micalculadora");
        cap.setCapability("appActivity", ".MainActivity");
        URL url = null;
        try {
            url = new URL("http://127.0.0.1:4723/wd/hub");
            driver = new AndroidDriver<MobileElement>(url, cap);
            System.out.println("Se ha iniciado la aplicación");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        inciarPruebas();
    }

    public void inciarPruebas() {
        MobileElement btn00 = driver.findElementById("mx.edu.utez.micalculadora:id/btn0");
        MobileElement btn01 = driver.findElementById("mx.edu.utez.micalculadora:id/btn1");
        MobileElement btn02 = driver.findElementById("mx.edu.utez.micalculadora:id/btn2");
        MobileElement btn03 = driver.findElementById("mx.edu.utez.micalculadora:id/btn3");
        MobileElement btn04 = driver.findElementById("mx.edu.utez.micalculadora:id/btn4");
        MobileElement btn05 = driver.findElementById("mx.edu.utez.micalculadora:id/btn5");
        MobileElement btn06 = driver.findElementById("mx.edu.utez.micalculadora:id/btn6");
        MobileElement btn07 = driver.findElementById("mx.edu.utez.micalculadora:id/btn7");
        MobileElement btn08 = driver.findElementById("mx.edu.utez.micalculadora:id/btn8");
        MobileElement btn09 = driver.findElementById("mx.edu.utez.micalculadora:id/btn9");

        MobileElement btnPunto = driver.findElementById("mx.edu.utez.micalculadora:id/btnPunto");

        MobileElement btnSumar = driver.findElementById("mx.edu.utez.micalculadora:id/btnSumar");
        MobileElement btnRestar = driver.findElementById("mx.edu.utez.micalculadora:id/btnRestar");
        MobileElement btnMultiplicar = driver.findElementById("mx.edu.utez.micalculadora:id/btnMultiplicacion");
        MobileElement btnDividir = driver.findElementById("mx.edu.utez.micalculadora:id/btnDivision");

        MobileElement btnResultado = driver.findElementById("mx.edu.utez.micalculadora:id/btnResultado");

        MobileElement btnLimpiar = driver.findElementById("mx.edu.utez.micalculadora:id/btnLimpiar");

        System.out.println("\tOPERACIÓN SUMA");
        btn05.click();
        btn00.click();
        btnSumar.click();
        btn05.click();
        btnResultado.click();
        Assert.assertEquals("55.0", driver.findElementById("mx.edu.utez.micalculadora:id/txtCampo").getText());
        btnLimpiar.click();

        System.out.println("\n\tOPERACIÓN RESTA");
        btn01.click();
        btn00.click();
        btn03.click();
        btnRestar.click();
        btn04.click();
        btn02.click();
        btn09.click();
        btnResultado.click();
        Assert.assertEquals("-326.0", driver.findElementById("mx.edu.utez.micalculadora:id/txtCampo").getText());
        btnLimpiar.click();

        System.out.println("\n\tOPERACIÓN MULTIPLICACIÓN");
        btn06.click();
        btnMultiplicar.click();
        btn08.click();
        btnResultado.click();
        Assert.assertEquals("48.0", driver.findElementById("mx.edu.utez.micalculadora:id/txtCampo").getText());
        btnLimpiar.click();

        System.out.println("\n\tOPERACIÓN DIVISION");
        btn01.click();
        btn05.click();
        btnPunto.click();
        btn05.click();
        btnDividir.click();
        btn05.click();
        btnResultado.click();
        Assert.assertEquals("3.1", driver.findElementById("mx.edu.utez.micalculadora:id/txtCampo").getText());
        btnLimpiar.click();
    }
}
