package TestAppium.Notas;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class TestNotas {

    static AndroidDriver<MobileElement> driver;

    @Test
    public void abrirNotas(){
        DesiredCapabilities cap = new DesiredCapabilities();

        // CAMBIA ESTAS TRES LINEAS CON LAS DE TU CELULAR
        cap.setCapability("deviceName", "ZY322NQQ55");
        cap.setCapability("platformName", "Android");
        cap.setCapability("platformVersion", "9");

        cap.setCapability("appPackage", "notepad.note.notas.notes.notizen");
        cap.setCapability("appActivity", ".main.SplashActivity");
//        cap.setCapability("appActivity", ".main.MainActivity");
        URL url = null;
        try {
            url = new URL("http://127.0.0.1:4723/wd/hub");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver = new AndroidDriver<MobileElement>(url, cap);
        System.out.println("Se ha iniciado la aplicación");


        try {
            Thread.sleep(3*1000);
            
            MobileElement btnAgregarNota = driver.findElementById("notepad.note.notas.notes.notizen:id/btnAddNote");
            btnAgregarNota.click();

            MobileElement btnNota = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView");
            btnNota.click();

            MobileElement txtTitulo = driver.findElementById("notepad.note.notas.notes.notizen:id/editTitle");
            txtTitulo.setValue("TestPrueba");

            MobileElement txtContenido = driver.findElementById("notepad.note.notas.notes.notizen:id/editContent");
            txtContenido.setValue("Esta es una prueba con Appium");

            MobileElement btnGuardar = driver.findElementById("notepad.note.notas.notes.notizen:id/btnAdd");

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
