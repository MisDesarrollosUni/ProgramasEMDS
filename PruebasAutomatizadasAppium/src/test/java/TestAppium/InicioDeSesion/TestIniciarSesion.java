package TestAppium.InicioDeSesion;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class TestIniciarSesion {

    static AndroidDriver<MobileElement> driver;

    @Test
    public void abrirAplicacion() {
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability("deviceName", "ZY322NQQ55");
        cap.setCapability("platformName", "Android");
        cap.setCapability("platformVersion", "9");
        //cmp=mx.edu.utez.miprimeraapp/.MainActivity
        cap.setCapability("appPackage", "mx.edu.utez.miprimeraapp");
        cap.setCapability("appActivity", ".MainActivity");
        URL url = null;
        try {
            url = new URL("http://127.0.0.1:4723/wd/hub");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver = new AndroidDriver<MobileElement>(url, cap);
        System.out.println("Se ha iniciado la aplicación");

        //ABRIMOS APPIUM PARA HACER CAPTURA A NUESTRA APP Y PODER MANEJARLA

        MobileElement txtUsuario = driver.findElementById("mx.edu.utez.miprimeraapp:id/txtUsuario");
        MobileElement txtContrasena = driver.findElementById("mx.edu.utez.miprimeraapp:id/txtContrasena");

        MobileElement btnIniciar = driver.findElementById("mx.edu.utez.miprimeraapp:id/btnIniciar");
        MobileElement btnLimpiar = driver.findElementById("mx.edu.utez.miprimeraapp:id/btnLimpiar");
        MobileElement txtResultado;

        txtUsuario.setValue("Hector");
        txtContrasena.setValue("123456");
        btnIniciar.click();
        txtResultado = driver.findElementById("mx.edu.utez.miprimeraapp:id/txtResultado");
        System.out.println("RESULTADO ESPERADO: CORRECTAMENTE");
        System.out.println("RESULTADO OBTENIDO: " + txtResultado.getText());
        Assert.assertEquals("CORRECTAMENTE", txtResultado.getText());

       /* btnLimpiar.click();

        txtUsuario.setValue("No existe");
        txtContrasena.setValue("123456");
        btnIniciar.click();
        txtResultado = driver.findElementById("mx.edu.utez.miprimeraapp:id/txtResultado");
        System.out.println("\nRESULTADO ESPERADO: CORRECTAMENTE");
        System.out.println("RESULTADO OBTENIDO: " + txtResultado.getText());
        Assert.assertEquals("CORRECTAMENTE", txtResultado.getText());*/


    }


}
