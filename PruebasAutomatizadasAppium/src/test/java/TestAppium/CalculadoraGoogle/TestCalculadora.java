package TestAppium.CalculadoraGoogle;

/*
*  Saldaña Espinoza Hector
* UTEZ - 4C - DSM
*/

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.net.MalformedURLException;
import java.net.URL;

public class TestCalculadora {

    static AndroidDriver<MobileElement> driver;

   /* @Before
    public void main() {
        try {
            abrirCalculadora(); // ABRIMOS LA CALCULADORA
        } catch (Exception e) {
            System.out.println(e);
        }
    }*/

    @Test
    public void abrirCalculadora() {
        DesiredCapabilities cap = new DesiredCapabilities();

        //ESTOS DATOS JSON SE COLOCAN EN APPIUM EN LA PARTE DE INSPECTOR SESSION
        cap.setCapability("deviceName", "ZY322NQQ55"); // COLOCAMOS MI DISPOSITIVO COMO EL CODIGO, ESO SE CONOCE EN EL ADB
        cap.setCapability("platformName", "Android"); // LA PLATAFORMA ANDROID
        cap.setCapability("platformVersion", "9"); // MI VERSION DEL DISPOSITIVO
        //cmp=com.google.android.calculator/com.android.calculator2.Calculator  // ESTO SE TIENE QUE BUSACR EN EL ADB EN LOGCAT $ adb logcat > [nombre_archiv.txt]
        cap.setCapability("appPackage", "com.google.android.calculator");  // SE SEPARA ANTES /
        cap.setCapability("appActivity", "com.android.calculator2.Calculator"); // Y DESPUES DE LA /

        URL url = null;
        try {
            url = new URL("http://127.0.0.1:4723/wd/hub"); // SE CONECTA AL SERVIDOR DE APPIUM, PARA ESO LO TENEMOS QUE TENER PRENDIDO ANTES DE HACER OTRA COSA
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver = new AndroidDriver<MobileElement>(url, cap);
        System.out.println("Se ha iniciado la aplicación");

        // CREAMOS VALORES CON LOS BOTONES, LOS BOTONES LOS ENCONTRAMOS EN APPIUM EN LA PARTE DE INSPECTOR SESSION

        /*------------------------------------BOTONES NÚMEROS---------------------------------------*/
        MobileElement btn01 = driver.findElement(By.id("com.google.android.calculator:id/digit_1"));
        MobileElement btn02 = driver.findElement(By.id("com.google.android.calculator:id/digit_2"));
        MobileElement btn03 = driver.findElement(By.id("com.google.android.calculator:id/digit_3"));
        MobileElement btn04 = driver.findElement(By.id("com.google.android.calculator:id/digit_4"));
        MobileElement btn05 = driver.findElement(By.id("com.google.android.calculator:id/digit_5"));
        MobileElement btn06 = driver.findElement(By.id("com.google.android.calculator:id/digit_6"));
        MobileElement btn07 = driver.findElement(By.id("com.google.android.calculator:id/digit_7"));
        MobileElement btn08 = driver.findElement(By.id("com.google.android.calculator:id/digit_8"));
        MobileElement btn09 = driver.findElement(By.id("com.google.android.calculator:id/digit_9"));
        MobileElement btn00 = driver.findElement(By.id("com.google.android.calculator:id/digit_0"));
        /*---------------------------------------------------------------------------*/

        /*------------------------------------PUNTO DECIMAL---------------------------------------*/
        MobileElement btnPoint = driver.findElement(By.id("com.google.android.calculator:id/dec_point"));
        /*---------------------------------------------------------------------------*/

        /*------------------------------------BOTONES OPERADORES---------------------------------------*/
        MobileElement btnSumar = driver.findElement(By.id("com.google.android.calculator:id/op_add"));
        MobileElement btnMultiplicar = driver.findElement(By.id("com.google.android.calculator:id/op_mul"));
        MobileElement btnRestar = driver.findElement(By.id("com.google.android.calculator:id/op_sub"));
        MobileElement btnDividir = driver.findElement(By.id("com.google.android.calculator:id/op_div"));
        /*---------------------------------------------------------------------------*/

        /*------------------------------------BOTON RESULTADO---------------------------------------*/
        MobileElement btnResultado = driver.findElement(By.id("com.google.android.calculator:id/eq"));
        /*---------------------------------------------------------------------------*/



        /*------------------------------------PRUEBA SUMAR---------------------------------------*/
        System.out.println("// OPERACION SUMAR");
        btn05.click();
        btn08.click();
        btnSumar.click();
        btn09.click();
        btnResultado.click();
        MobileElement resultadoOp = driver.findElementById("com.google.android.calculator:id/result_final");
        System.out.println("RESULTADO ESPERADO: 67");
        System.out.println("RESULTADO OBTENIDO: "+resultadoOp.getText());
        Assert.assertEquals("67", resultadoOp.getText());
        MobileElement btnLimpiar = driver.findElement(By.id("com.google.android.calculator:id/clr")); // SE COLOCA HASTA AQUI PORQUE ANTES NO SE RECONOCE
        btnLimpiar.click();
        /*---------------------------------------------------------------------------*/



        /*------------------------------------PRUEBA RESTAR---------------------------------------*/
        System.out.println("\n// OPERACION RESTA");
        btn02.click();
        btn07.click();
        btnRestar.click();
        btn06.click();
        btnResultado.click();
        resultadoOp = driver.findElementById("com.google.android.calculator:id/result_final");
        System.out.println("RESULTADO ESPERADO: 21");
        System.out.println("RESULTADO OBTENIDO: "+resultadoOp.getText());
        Assert.assertEquals("21", resultadoOp.getText());
        btnLimpiar.click();
        /*---------------------------------------------------------------------------*/



        /*------------------------------------PRUEBA MULTIPLICAR---------------------------------------*/
        System.out.println("\n// OPERACION MULTIPLICAR");
        btn01.click();
        btn00.click();
        btnMultiplicar.click();
        btn05.click();
        btnResultado.click();
        resultadoOp = driver.findElementById("com.google.android.calculator:id/result_final");
        System.out.println("RESULTADO ESPERADO: 50");
        System.out.println("RESULTADO OBTENIDO: "+resultadoOp.getText());
        Assert.assertEquals("50", resultadoOp.getText());
        btnLimpiar.click();
        /*---------------------------------------------------------------------------*/



        /*------------------------------------PRUEBA DIVIDIR---------------------------------------*/
        System.out.println("\n// OPERACION DIVIDIR");
        btn01.click();
        btn00.click();
        btn00.click();
        btnDividir.click();
        btn01.click();
        btn00.click();
        btn00.click();
        btnResultado.click();
        resultadoOp = driver.findElementById("com.google.android.calculator:id/result_final");
        System.out.println("RESULTADO ESPERADO: 1");
        System.out.println("RESULTADO OBTENIDO: "+resultadoOp.getText());
        Assert.assertEquals("1", resultadoOp.getText());
        btnLimpiar.click();
        /*---------------------------------------------------------------------------*/


        // CHECAR ESTA LINEA CON LA QUE LA PANTALLA NOS ARROJE Y VEIFICAR SI ES EL MISMO RESULTADO
        //Assert.assertEquals(4,driver.findElementById("com.sec.android.app.popupcalculator:id/txtCalc").getText());


    }
}
