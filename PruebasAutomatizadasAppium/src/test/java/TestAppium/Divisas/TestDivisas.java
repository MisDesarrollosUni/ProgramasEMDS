package TestAppium.Divisas;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class TestDivisas {
    static AndroidDriver<MobileElement> driver;

    @Test
    public void abrirDivisas(){
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability("deviceName", "ZY322NQQ55");
        cap.setCapability("platformName", "Android");
        cap.setCapability("platformVersion", "9");
        //cmp=mx.edu.utez.calculadordivisas/.Main
        cap.setCapability("appPackage", "mx.edu.utez.calculadordivisas");
        cap.setCapability("appActivity", ".Main");
        URL url = null;
        try {
            url = new URL("http://127.0.0.1:4723/wd/hub");
            driver = new AndroidDriver<MobileElement>(url, cap);
            System.out.println("Se ha iniciado la aplicación");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        MobileElement spinerUno = driver.findElementById("mx.edu.utez.calculadordivisas:id/spnDivisa1");
        MobileElement spinerDos = driver.findElementById("mx.edu.utez.calculadordivisas:id/spnDivisa2");


        MobileElement txtCantidad = driver.findElementById("mx.edu.utez.calculadordivisas:id/txtCantidad");
        MobileElement txtResultado = driver.findElementById("mx.edu.utez.calculadordivisas:id/txtResultado");

        MobileElement btnConvertir = driver.findElementById("mx.edu.utez.calculadordivisas:id/btnConvertir");
        MobileElement btnLimpiar = driver.findElementById("mx.edu.utez.calculadordivisas:id/btnLimpiar");
        MobileElement MXN,USD,EUR,JPY,GBP;
        String resulEsperado;

        txtCantidad.setValue("150");
        spinerUno.click();
        MXN = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[2]");
        MXN.click();
        spinerDos.click();
        USD = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[3]");
        USD.click();
        btnConvertir.click();
        resulEsperado = "$ 7.35 $";
        System.out.println("RESULTADO ESPERADO: "+resulEsperado);
        System.out.println("RESULTADO OBTENIDO: "+txtResultado.getText());
        Assert.assertEquals(resulEsperado, txtResultado.getText());
        btnLimpiar.click();

        txtCantidad.setValue("5");
        spinerUno.click();
        EUR = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[4]");
        EUR.click();
        spinerDos.click();
        JPY = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[5]");
        JPY.click();
        btnConvertir.click();
        resulEsperado = "$ 613.70 ¥";
        System.out.println("\nRESULTADO ESPERADO: "+resulEsperado);
        System.out.println("RESULTADO OBTENIDO: "+txtResultado.getText());
        Assert.assertEquals(resulEsperado, txtResultado.getText());
        btnLimpiar.click();

     /*   txtCantidad.setValue("20");
        spinerUno.click();
        GBP = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[6]");
        GBP.click();
        spinerDos.click();
        MXN = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[2]");
        MXN.click();
        btnConvertir.click();
        resulEsperado = "$ 540 MXN";
        System.out.println("\nRESULTADO ESPERADO: "+resulEsperado);
        System.out.println("RESULTADO OBTENIDO: "+txtResultado.getText());
        Assert.assertEquals(resulEsperado, txtResultado.getText());
        btnLimpiar.click();*/



    }
}
