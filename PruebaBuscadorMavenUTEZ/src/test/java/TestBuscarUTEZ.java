/*
 * Saldaña Espinoza Hector
 * 4C - UTEZ
 * Desarrollo de Software Multiplataforma
 * Evaluación y mejora de Software
 */

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class TestBuscarUTEZ {

	private WebDriver driver;

	@Before
	public void setUp(){
		System.setProperty("webdriver.chrome.driver","C:\\Users\\hecto\\Escritorio\\Universidad\\ProgramasEMDS\\PruebaBuscadorMavenUTEZ\\src\\utilidades\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.google.com");
	}

	@Test
	public void buscarUTEZ(){
		//SE BUSCA EL ELEMENTO EN GOOGLE, EN NUESTRO CASO ES q ES EL NOMBRE DEL INPUT DE GOOGLE PARA BUSCAR (asi le pusieron)
		WebElement textoBuscar=driver.findElement(By.name("q"));
		textoBuscar.clear();
		textoBuscar.sendKeys("YOU TUBE");
		textoBuscar.submit();


		//PONER TIEMPO EN CADA ACCION
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		// EN CADA BUSQUEDA INSPECCIONAMOS ELEMENTO Y BUSCAMOS EN HEAD EL TITTLE
		// COMPROBAMOS SI LA BUSQUEDA DEL TITULO DEL DRIVER ES IGUAL AL QUE ESPERAMOS
		assertEquals("YOU TUBE - Buscar con Google",driver.getTitle());
	}



}
