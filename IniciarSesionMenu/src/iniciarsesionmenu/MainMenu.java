/*
 * SALDAÑA ESPINOZA HECTOR | 4C
 * EVALUACIÓN Y MEJORA PARA EL DESARROLLO DE SOFTWARE

 * DESCRIPCIÓN DEL PROBLEMA
   1.- Programar un inicio de sesión con lo siguiente:
        Debe ser estático con un usuario correcto que se valide solamente:
        Usuario:  debe contener un email con cualquier dominio (ejemplo, dominio @gmail.com)
        Contraseña: Letras solo a-z o A-Z y números 0-9
        Pueden utilizar algún patrón con matches.
   2.- Si el usuario y contraseña coincide deben entrar a un menú con lo siguiente:
        MENÚ:

        Selecciona una opción para ejecutar el cálculo:
        1.- Calcular 5% de descuento de cualquier cantidad.
        2.- Calcular el 16% de IVA de una cantidad.
        3.- Calcular la raíz cuadrada de un número.
        4.- Calcular el factorial de un número.
        5.- Salir
   3.- Deben programar la clase main, clase métodos de cada cálculo por separado, 
   y clase de test con los métodos de prueba para los métodos de los cálculos.

   NOTA: El método main() no lleva casos de prueba.

   En este ejercicio deben generar 3 casos de prueba: 1 exitoso y 2 que sean sin éxito.
*/
package iniciarsesionmenu;


public class MainMenu {
   
    public static void main(String[] args) {
      ValidacionesSesion sesion = new ValidacionesSesion();
      sesion.inicioSesion();
    }
    
}
