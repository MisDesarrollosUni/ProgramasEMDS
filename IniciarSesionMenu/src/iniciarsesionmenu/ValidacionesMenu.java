package iniciarsesionmenu;

import java.util.Scanner;

public class ValidacionesMenu {

    private Scanner leer = new Scanner(System.in);
    private String sesionActiva;

    public ValidacionesMenu(String sesionActiva) {
        this.sesionActiva = sesionActiva;
    }

    public ValidacionesMenu() {

    }

    public void menu() {
        int opc = 0;
        double numEvaluar = 0.0, resultado = 0.0;
        do {
            try {
                System.out.println("      <̶<̶<̶°• MENÚ •°>̶>̶>̶");
                System.out.println("Usuario:\t" + sesionActiva);
                System.out.println("\t1.- Calcular 5% de descuento de cualquier cantidad");
                System.out.println("\t2.- Calcular el 16% de IVA de una cantidad");
                System.out.println("\t3.- Calcular la raíz cuadrada de un número");
                System.out.println("\t4.- Calcular el factorial de un número.");
                System.out.println("\t5.- Salir");
                System.out.print("Eliga: ");
                opc = leer.nextInt();
                if (opc == 1 || opc == 2) {
                    System.out.print("Ingrese la cantidad: ");
                    numEvaluar = validarNumero();
                } else if (opc == 3 || opc == 4) {
                    System.out.print("Ingrese el número: ");
                    numEvaluar = validarNumeroEntero();
                }
                switch (opc) {
                    case 1:
                        resultado = calcularDescuento(numEvaluar);
                        break;
                    case 2:
                        resultado = calcularIVA(numEvaluar);
                        break;
                    case 3:
                        resultado = calcularRaizCuadrada(numEvaluar);
                        break;
                    case 4:
                        resultado = calcularFactorial(numEvaluar);
                        break;
                    case 5:
                        opc = 5;
                        System.out.println("SALIENDO..");
                        break;
                    default:
                        System.out.println("*No existe esa opción");
                }
                if (opc >= 1 && opc <= 4) {
                    System.out.println("\tEl resultado es: " + resultado);
                }
            } catch (Exception e) {
                System.out.println("*SOLO NÚMEROS PARA ELEGIR");
                leer.next();
            }
            System.out.println();
        } while (opc != 5);

    }

    public double calcularDescuento(double _cantidad) {
        double descuento = 0.0, PORCIENTO = 0.05;
        descuento = _cantidad * PORCIENTO;
        return descuento;
    }

    public double calcularIVA(double _cantidad) {
        double aumento = 0.0, IVA = 0.16;
        aumento = _cantidad * IVA;
        return aumento;
    }

    public double calcularRaizCuadrada(double _numero) {
        double raiz = 0.0;
        raiz = Math.sqrt(_numero);
        return raiz;
    }

    public double calcularFactorial(double _numero) {
        double factorial = 1;
        for (int i = 1; i <= _numero; i++) {
            factorial *= i;
        }
        return factorial;
    }

    public int validarNumeroEntero() {
        boolean flag = false;
        int _num = 0;
        do {
            try {
                _num = leer.nextInt();
                if (_num > 0) {
                    flag = true;
                }else{
                    System.out.println("\t*Solo números positivos");
                    System.out.print(": ");
                }
            } catch (Exception e) {
                System.out.println("\t*Solo números enteros válidos");
                System.out.print(": ");
                leer.next();
            }
        } while (!flag);
        return _num;
    }

    public double validarNumero() {
        boolean flag = false;
        double _cantidad = 0.0;
        do {
            try {
                _cantidad = leer.nextDouble();
                if (_cantidad > 0) {
                    flag = true;
                }else{
                    System.out.println("\t*Solo números positivos");
                    System.out.print(": ");
                }
            } catch (Exception e) {
                System.out.println("\t*Solo números válidos");
                System.out.print(": ");
                leer.next();
            }
        } while (!flag);
        return _cantidad;
    }

}
