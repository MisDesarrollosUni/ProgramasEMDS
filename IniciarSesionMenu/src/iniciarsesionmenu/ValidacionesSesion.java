package iniciarsesionmenu;

import java.awt.HeadlessException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

public class ValidacionesSesion extends Cuentas {

    private final Pattern patronEspec = Pattern.compile("[^A-Za-z0-9]");
    private final Pattern patronContraseña = Pattern.compile("[a-zA-Z0-9]*");
    private final Pattern patronCorreo = Pattern.compile("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
    private String email, pass, nombreCompleto;

    public void inicioSesion() {
        crearCuentas();
        if (setCorreo() && setContraseña()) {
            validarInicioSesion();
            if (nombreCompleto != null) {
                ValidacionesMenu menu = new ValidacionesMenu(nombreCompleto);
                menu.menu();
            } else {
                System.out.println("\t█▀▀ █▀ ▀█▀ ▄▀█   █▀▀ █░█ █▀▀ █▄░█ ▀█▀ ▄▀█   █▄░█ █▀█   █▀▀ ▀▄▀ █ █▀ ▀█▀ █▀▀\n\t"
                        + "██▄ ▄█ ░█░ █▀█   █▄▄ █▄█ ██▄ █░▀█ ░█░ █▀█   █░▀█ █▄█   ██▄ █░█ █ ▄█ ░█░ ██▄");
                System.out.println("\t*Intentelo de nuevo");
            }
        }
    }

    public boolean setCorreo() {
        boolean flag = false;
        try {
            do {
                email = JOptionPane.showInputDialog(null, "Ingrese correo:\nEjemplo: usuario@dominio.com", "Inicio de Sesión", JOptionPane.QUESTION_MESSAGE);
                if (email.length() >= 15 && email.length() <= 50) {
                    flag = validarCorreo(email);
                } else {
                    System.out.println("*El correo debe tener mínimo 15 caracteres");
                }
            } while (!flag);
        } catch (HeadlessException e) {
            System.out.println("SALIENDO..");
        } finally {
            return flag;
        }
    }

    public boolean validarCorreo(String _email) {
        boolean flag = false;
        _email = _email.trim();
        Matcher match = patronCorreo.matcher(_email);
        if (match.find()) {
            flag = true;
        } else {
            System.out.println("*No es correo válido");
        }
        return flag;
    }

    public boolean setContraseña() {
        boolean flag = false;
        try {
            do {
                pass = JOptionPane.showInputDialog(null, "Ingrese contraseña:\n{" + email + "}", "Inicio de Sesión", JOptionPane.QUESTION_MESSAGE);
                if (pass.length() >= 8) {
                    flag = validarContraseña(pass);
                } else {
                    System.out.println("*La contraseña debe tener como mínimo 8 caracteres");
                }
            } while (!flag);

        } catch (HeadlessException e) {
            System.out.println("SALIENDO..");
        } finally {
            return flag;
        }
    }

    public boolean validarContraseña(String _pass) {
        boolean flag = false, band = false;
        Matcher match = patronContraseña.matcher(_pass);
        if (match.find()) {
            flag = true;
        }
        match = patronEspec.matcher(_pass);
        if (!match.find()) {
            band = true;
        } else {
            System.out.println("*No es una contraseña válida");
        }
        return flag && band;

    }

    public void validarInicioSesion() {
        for (int i = 0; i < personas.size(); i++) {
            if (email.equalsIgnoreCase(personas.get(i).getEmail()) && (pass.equals(personas.get(i).getContraseña()))) {
                nombreCompleto = personas.get(i).getNombreCompleto();
                break;
            }
        }
    }
}
