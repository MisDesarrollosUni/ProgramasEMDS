
package iniciarsesionmenu;

import java.util.ArrayList;
import java.util.List;

public class Cuentas {
    
    static List<PersonaBean> personas = new ArrayList<>();
    
    public void crearCuentas(){
        personas.add(new PersonaBean("hector@gmail.com", "Hector0001","Hector Saldaña"));
        personas.add(new PersonaBean("sebas@gmail.com", "Sebas0002","Sebastian Carvajal"));
        personas.add(new PersonaBean("gandy@gmail.com", "Gandy0003", "Gandy Avila"));
        personas.add(new PersonaBean("raul@gmail.com", "Raul0004", "Raúl Adame"));
        personas.add(new PersonaBean("alexis@gmail.com", "Alexis0005", "Alexis Loya"));
        personas.add(new PersonaBean("berenice@gmail.com", "Berenice0006", "Berenice Torres"));
    }
    
}
