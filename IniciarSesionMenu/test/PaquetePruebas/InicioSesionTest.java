/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PaquetePruebas;

import iniciarsesionmenu.ValidacionesMenu;
import iniciarsesionmenu.ValidacionesSesion;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hector Saldaña
 */
public class InicioSesionTest {

    public InicioSesionTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    ValidacionesSesion inicio = new ValidacionesSesion();
    ValidacionesMenu menu = new ValidacionesMenu();

    @Test
    public void testValidarCorreo1() {
        System.out.println("Test 1 para validar correo EXITO");
        String email = "hector@gmail.com";
        boolean resulEsperado = true;
        boolean resulObtenido = inicio.validarCorreo(email);
        assertEquals(resulEsperado, resulObtenido);
        System.out.println();
    }

    @Test
    public void testValidarCorreo2() {
        System.out.println("Test 2 para validar correo FALLO");
        String email = "juanperez@@gmail.com";
        boolean resulEsperado = true;
        boolean resulObtenido = inicio.validarCorreo(email);
        assertEquals(resulEsperado, resulObtenido);
        System.out.println();

    }

    @Test
    public void testValidarContraseña1() {
        System.out.println("Test 1 para validar contraseña EXITO");
        String contraseña = "Hector001";
        boolean resulEsperado = true;
        boolean resulObtenido = inicio.validarContraseña(contraseña);
        assertEquals(resulEsperado, resulObtenido);
        System.out.println();

    }

    @Test
    public void testValidarContraseña2() {
        System.out.println("Test 2 para validar contraseña FALLO");
        String contraseña = "hecto#$$#";
        boolean resulEsperado = true;
        boolean resulObtenido = inicio.validarContraseña(contraseña);
        assertEquals(resulEsperado, resulObtenido);
        System.out.println();
    }

    @Test
    public void testCalcularRaizCuadrada() {
        System.out.println("Test 1 para calcular raíz cuadrada EXITO");
        double numero = 64;
        double resulEsperado = 8;
        double resulObtenido = menu.calcularRaizCuadrada(numero);
        assertEquals(resulEsperado, resulObtenido, 0);
        System.out.println();
    }

    @Test
    public void testCalcularFactorial() {
        System.out.println("Test 1 para calcular factorial EXITO");
        double numero = 5;
        double resulEsperado = 120;
        double resulObtenido = menu.calcularFactorial(numero);
        assertEquals(resulEsperado, resulObtenido, 0);
        System.out.println();
    }

}
