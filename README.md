# 4C | ProgramasEMDS
## Evaluación y Mejora para el Desarrollo de Software | 
> Saldaña Espinoza Hector - Desarrollo de Software
>

## Contenido 🚀
### NetBeans 8.2 ⚙️
* [InicioDeSesion](https://github.com/HectorSaldes/ProgramasEMDS/tree/master/InicioDeSesion) - Programa qe válida el inicio de sesión institucional
* [MiPrimeraPrueba](https://github.com/HectorSaldes/ProgramasEMDS/tree/master/MiPrimeraPrueba) - Uso de pruebas con JUnit
* [PruebaOperacionMaven](https://github.com/HectorSaldes/ProgramasEMDS/tree/master/PruebaOperacionesMaven) - Uso de pruebas con JUnit con Maven (No me funciono) IniciarSesionMenu
* [IniciarSesionMenu](https://github.com/HectorSaldes/ProgramasEMDS/tree/master/IniciarSesionMenu) - Login con cualquier correo y al ingresar mostrar un menú con varias acciones
* [ProgramasVarios](https://github.com/HectorSaldes/ProgramasEMDS/tree/master/ProgramasVarios) - Pruebas practicas para examen unidad 2
* [ExamenUnidad2](https://github.com/HectorSaldes/ProgramasEMDS/tree/master/ExamenUnidad2) - Realizar 3 ejercicios con sus recpectivas pruebas JUnit

### InteliJ 2020.1 🐱‍👤
* [PruebaBuscadorMavenUTEZ](https://github.com/HectorSaldes/ProgramasEMDS/tree/master/PruebaBuscadorMavenUTEZ) - Pruebas automatizadas en Google Chrome
* [PruebasAutomatizadas](https://github.com/HectorSaldes/ProgramasEMDS/tree/master/PruebasAutomatizadas) - Pruebas automatizadas con Google Chrome y ChromeDriver usando Katalon y Selenium IDE (extensiones)
* [TestCalculadoraAppium](https://github.com/HectorSaldes/ProgramasEMDS/tree/master/TestCalculadoraAppium) - Pruebas automatizadas a mi celular en la Calculadora, funciona Sumar, restar, multiplicar y dividir