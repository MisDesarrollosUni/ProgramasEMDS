/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IVA;

import IVA.CantidadIVA;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hector Saldaña
 */
public class CantidadIVATest {
    
    public CantidadIVATest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calcularIVA method, of class CantidadIVA.
     */
    @Test
    public void testCalcularIVA1() {
        System.out.println("calcularIVA PRUEBA EXITOSA");
        double cantidad = 500;
        CantidadIVA instance = new CantidadIVA();
        double expResult = 80;
        double result = instance.calcularIVA(cantidad);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }
    
    @Test
    public void testCalcularIVA2() {
        System.out.println("calcularIVA PRUEBA FALLIDA");
        double cantidad = 1054;
        CantidadIVA instance = new CantidadIVA();
        double expResult = 8;
        double result = instance.calcularIVA(cantidad);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }
    
    @Test
     public void testCalcularIVA3() {
        System.out.println("calcularIVA PRUEBA FALLIDA");
        double cantidad = 1845;
        CantidadIVA instance = new CantidadIVA();
        double expResult = 104;
        double result = instance.calcularIVA(cantidad);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }
    
    
}
