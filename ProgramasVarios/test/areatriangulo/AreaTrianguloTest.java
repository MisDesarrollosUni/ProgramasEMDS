package areatriangulo;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hector Saldaña
 */
public class AreaTrianguloTest {

    public AreaTrianguloTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of main method, of class AreaTriangulo.
     *
     * @Test public void testMain() { System.out.println("main"); String[] args
     * = null; AreaTriangulo.main(args); // TODO review the generated test code
     * and remove the default call to fail. fail("The test case is a
     * prototype."); }
     */
    /**
     * Test of calcularAreaTriangulo method, of class AreaTriangulo.
     */
    @Test
    public void testCalcularAreaTriangulo() {
        System.out.println("calcularAreaTriangulo");
        double base = 10.0;
        double altura = 2.0;
        AreaTriangulo instance = new AreaTriangulo();
        double expResult = 10.0;
        double result = instance.calcularAreaTriangulo(base, altura);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of main method, of class AreaTriangulo.
     *
     * @Test public void testMain() { System.out.println("main"); String[] args
     * = null; AreaTriangulo.main(args); // TODO review the generated test code
     * and remove the default call to fail. fail("The test case is a
     * prototype."); }
     */
    /**
     * Test of calcularAreaRectngulo method, of class AreaTriangulo.
     */
    @Test
    public void testCalcularAreaRectngulo() {
        System.out.println("calcularAreaRectngulo");
        double base = 8.0;
        double altura = 5.0;
        AreaTriangulo instance = new AreaTriangulo();
        double expResult = 40.0;
        double result = instance.calcularAreaRectngulo(base, altura);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

}
