/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IVA;

import java.util.Scanner;

/**
 *
 * @author Hector Saldaña
 */
public class CantidadIVA {
    
    
    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        double cantidad = 0;
        System.out.print("¿Qué cantidad desea conocer el IVA?: ");
        cantidad = leer.nextDouble();
        System.out.println("El IVA es: " + calcularIVA(cantidad));
    }
    
    public static double calcularIVA(double cantidad){
        return cantidad *0.16;
    }
    
}
