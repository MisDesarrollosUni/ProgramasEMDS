package mx.edu.utez.pruebaoperacionesmaven;

/**
 *
 * @author Hector Saldaña
 */
public class MetodosOperaciones {

    public int sumarNumeros(int a, int b) {
        return a + b;
    }
    
    public int restarNumeros(int a, int b) {
        return a - b;
    }
    
    public int dividirNumeros(int a, int b) {
        return a / b;
    }
    
    public int multiplicarNumeros(int a, int b) {
        return a * b;
    }
    
    
}
