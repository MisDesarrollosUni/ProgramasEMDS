/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utez.pruebaoperacionesmaven;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hector Saldaña
 */
public class MetodosOperacionesTest {
    
    public MetodosOperacionesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod() {
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of sumarNumeros method, of class MetodosOperaciones.
     */
    @Test
    public void testSumarNumeros() {
        System.out.println("sumar Numeros");
        int a = 5;
        int b = 5;
        MetodosOperaciones instance = new MetodosOperaciones();
        int expResult = 10;
        int result = instance.sumarNumeros(a, b);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of restarNumeros method, of class MetodosOperaciones.
     */
    @Test
    public void testRestarNumeros() {
        System.out.println("restar Numeros");
        int a = 10;
        int b = 5;
        MetodosOperaciones instance = new MetodosOperaciones();
        int expResult = 5;
        int result = instance.restarNumeros(a, b);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of dividirNumeros method, of class MetodosOperaciones.
     */
    @Test
    public void testDividirNumeros() {
        System.out.println("dividir Numeros");
        int a = 10;
        int b = 5;
        MetodosOperaciones instance = new MetodosOperaciones();
        int expResult = 2;
        int result = instance.dividirNumeros(a, b);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of multiplicarNumeros method, of class MetodosOperaciones.
     */
    @Test
    public void testMultiplicarNumeros() {
        System.out.println("multiplicarNumeros");
        int a = 3;
        int b = 2;
        MetodosOperaciones instance = new MetodosOperaciones();
        int expResult = 6;
        int result = instance.multiplicarNumeros(a, b);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
