/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PaquetePruebas;

import InicioSesion.Validaciones;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hector Saldaña
 */
public class InicioSesionTest {
    
    public InicioSesionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    //@Test
    //public void hello() {}
    Validaciones validar = new Validaciones();
    @Test
    public void testValidarCorreo1(){
        System.out.println("Test 1 para validar correo EXITO");
        String email = "20193tn070@utez.edu.mx";
        boolean resulEsperado = true;
        boolean resulObtenido = validar.validarCorreo(email);
        assertEquals(resulEsperado, resulObtenido);
    }
    
    @Test
    public void testValidarCorreo2(){
        System.out.println("Test 2 para validar correo FALLIDO");
        String email = "20193tn070@gmail.com";
        boolean resulEsperado = true;
        boolean resulObtenido = validar.validarCorreo(email);
        assertEquals(resulEsperado, resulObtenido);
    }
    @Test
    public void testValidarCorreo3(){
        System.out.println("Test 3 para validar correo FALLIDO");
        String email = "@utez.edu.mx";
        boolean resulEsperado = true;
        boolean resulObtenido = validar.validarCorreo(email);
        assertEquals(resulEsperado, resulObtenido);
    }
    
    @Test
    public void testValidarPassword1(){
        System.out.println();
        System.out.println("Test 1 para validar contraseña EXITO");
        String password = "contraseñA3$#23";
        boolean resulEsperado = true; 
        boolean resulObtenido = validar.validarPassword(password);
        assertEquals(resulEsperado, resulObtenido);
    }
    
    @Test
    public void testValidarPassword2(){
        System.out.println("Test 2 para validar contraseña FALLO");
        String password = "contraseñacontexto";
        boolean resulEsperado = true; 
        boolean resulObtenido = validar.validarPassword(password);
        assertEquals(resulEsperado, resulObtenido);
    }
    
    @Test
    public void testValidarPassword3(){
        System.out.println("Test 3 para validar contraseña FALLO");
        String password = "44546498787";
        boolean resulEsperado = true; 
        boolean resulObtenido = validar.validarPassword(password);
        assertEquals(resulEsperado, resulObtenido);
    }
    
    
}
