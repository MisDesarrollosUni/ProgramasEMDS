/*
 * SALDAÑA ESPINOZA HECTOR | 4C
 * EVALUACIÓN Y MEJORA PARA EL DESARROLLO DE SOFTWARE
 */
package InicioSesion;

import java.awt.HeadlessException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

public class Validaciones extends Cuentas {

    private final Pattern patronEspec = Pattern.compile("[^A-Za-z0-9]");
    private final Pattern patronNumero = Pattern.compile("[0-9]");
    private final Pattern patronMayus = Pattern.compile("[A-Z]");
    private final Pattern patronCorreo = Pattern.compile(".*@UTEZ.EDU.MX");
    private String email, pass, nombreCompleto;

    public boolean setCorreo() {
        boolean flag = false;
        try {
            do {
                email = JOptionPane.showInputDialog(null, "Ingrese correo:\nEjemplo: usuario@utez.edu.mx", "Inicio de Sesión", JOptionPane.QUESTION_MESSAGE);
                if (!email.isEmpty() && email.length() <= 30) {
                    flag = validarCorreo(email);
                } else {
                    System.out.println("*El correo no debe estar vacío y no debe ser muy extenso");
                }
            } while (!flag);

        } catch (HeadlessException e) {
            System.out.println("SALIENDO..");
        } finally {
            return flag;
        }
    }

    public boolean validarCorreo(String _email) {
        _email = _email.toUpperCase();
        _email = _email.trim();
        Matcher match = patronCorreo.matcher(_email);
        boolean valInstitucional = false, valArriba = false, valCorreo = false, valParams = false, flag = false;
        String[] noreply = _email.split("@");
        String isUTEZ = "UTEZ", isEDU = "EDU", isMX = "MX";
        int contUT = 0, contEDU = 0, contMX = 0;

        if (!_email.startsWith("@")) {
            valArriba = true;
        }
        if (match.find()) {
            valCorreo = true;
        }
        if (noreply.length == 2) {
            valInstitucional = true;
        }
        while (_email.indexOf(isUTEZ) > -1) {
            _email = _email.substring(_email.indexOf(isUTEZ) + isUTEZ.length(), _email.length());
            contUT++;
        }
        while (_email.indexOf(isEDU) > -1) {
            _email = _email.substring(_email.indexOf(isEDU) + isEDU.length(), _email.length());
            contEDU++;
        }
        while (_email.indexOf(isMX) > -1) {
            _email = _email.substring(_email.indexOf(isMX) + isMX.length(), _email.length());
            contMX++;
        }

        if (contUT == 1 && contEDU == 1 && contMX == 1) {
            valParams = true;
        }
        flag = valArriba && valCorreo && valInstitucional && valParams;
        if (!flag) {
            System.out.println("*No es un correo institucional válido");
        }
        return flag;

    }

    public boolean setPassword() {
        boolean flag = false;
        try {
            do {
                pass = JOptionPane.showInputDialog(null, "Ingrese contraseña:\n{" + email + "}", "Inicio de Sesión", JOptionPane.QUESTION_MESSAGE);
                if (!pass.isEmpty() && pass.length() >= 8 && pass.length() <= 25) {
                    flag = validarPassword(pass);
                } else {
                    System.out.println("*La contraseña no debe estar vacía, al menos debe tener entre 8 y 30 carácteres");
                }
            } while (!flag);
        } catch (HeadlessException e) {
            System.out.println("SALIENDO..");
        } finally {
            return flag;
        }
    }

    public boolean validarPassword(String _pass) {
        boolean filNumero = false, filMayus = false, filEspecial = false;
        Matcher match = patronNumero.matcher(_pass);
        if (match.find()) {
            filNumero = true;
        } else {
            System.out.println("*La contraseña debe tener al menos un número");
        }

        match = patronMayus.matcher(_pass);
        if (match.find()) {
            filMayus = true;
        } else {
            System.out.println("*La contraseña debe tener al menos una mayúscula");
        }

        match = patronEspec.matcher(_pass);
        if (match.find()) {
            filEspecial = true;
        } else {
            System.out.println("*La contraseña debe contener al menos un carácter especial");
        }

        return filNumero && filMayus && filEspecial;
    }

    public void validarInicioSesion() {
        boolean flag = false;
        crearCuentas();
        for (int i = 0; i < personas.size(); i++) {
            if (email.equalsIgnoreCase(personas.get(i).getEmail()) && (pass.equals(personas.get(i).getContraseña()))) {
                flag = true;
                nombreCompleto = personas.get(i).getNombreCompleto();
                break;
            }
        }
        if (flag) {
            JOptionPane.showMessageDialog(null, "Bienvenido: " + nombreCompleto, "Inicio de Sesión", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "INCORRECTO", "Inicio de Sesión", JOptionPane.ERROR_MESSAGE);
        }
    }

}
