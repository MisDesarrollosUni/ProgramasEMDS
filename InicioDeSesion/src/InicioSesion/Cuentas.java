/*
 * SALDAÑA ESPINOZA HECTOR | 4C
 * EVALUACIÓN Y MEJORA PARA EL DESARROLLO DE SOFTWARE
 */


package InicioSesion;

import java.util.ArrayList;
import java.util.List;

public class Cuentas {

    static List<PersonaBean> personas = new ArrayList<>();

    public void crearCuentas() {
        personas.add(new PersonaBean("20193tn070@utez.edu.mx", "Hector070+","Hector Saldaña"));
        personas.add(new PersonaBean("20193tn010@utez.edu.mx", "Sebastian010+","Sebastian Carvajal"));
        personas.add(new PersonaBean("20193tn083@utez.edu.mx", "Gandy083+", "Gandy Avila"));
        personas.add(new PersonaBean("20193tn147@utez.edu.mx", "Raulcito147+", "Raúl Adame"));
        personas.add(new PersonaBean("20193tn025@utez.edu.mx", "Alexisloya025+", "Alexis Loya"));
        personas.add(new PersonaBean("20193tn173@utez.edu.mx", "Berenice173+", "Berenice Torres"));
        
    }

}
