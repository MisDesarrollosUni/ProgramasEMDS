/*
 * SALDAÑA ESPINOZA HECTOR | 4C
 * EVALUACIÓN Y MEJORA PARA EL DESARROLLO DE SOFTWARE

 * DESCRIPCIÓN DEL PROBLEMA
   Se cuenta con un inicio de sesión que dadas las siguientes especificaciones:
    1.- Debe contar con dos campos o solicitar: usuario y contraseña.
    2.- Debe estar registrado o existir la cuenta con la que debe ingresar.
    3.- El usuario deberá ser un correo con el siguiente formato:  usuario@utez.edu.mx (únicamente el dominio que se indica).
    4.- La contraseña deberá validar que cuente con: mínimo 8 caracteres, deberá tener al menos un numero, una letra mayúscula 
    y un carácter especial (/*+._-?$%]()). 
    Deben generar y subir en forma de lista las posibles pruebas (casos de prueba) que deberán aplicarse al Inicio de sesión, 
    para probar la funcionalidad correcta en un ambiente de pruebas unitarias.
 */
package InicioSesion;

public class MainLogin {

    public static void main(String[] args) {
        Validaciones validar = new Validaciones();
        if (validar.setCorreo() && validar.setPassword()) {
            validar.validarInicioSesion();
        }

    }
}
