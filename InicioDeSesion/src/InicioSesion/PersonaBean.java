/*
 * SALDAÑA ESPINOZA HECTOR | 4C
 * EVALUACIÓN Y MEJORA PARA EL DESARROLLO DE SOFTWARE
 */

package InicioSesion;

public class PersonaBean {
    private String email;
    private String contraseña;
    private String nombreCompleto;

    public PersonaBean(String email, String contraseña, String nombreCompleto) {
        this.email = email;
        this.contraseña = contraseña;
        this.nombreCompleto = nombreCompleto;
    }

    public PersonaBean() {
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }
    
    
}
